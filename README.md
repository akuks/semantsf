<h1> SemantSF </h2>
<p> SemantSF is a web application for NGO named as SemantSewa Foundation </p>


<h2> Pre-requisites </h2>

* Composer must be installed on your system. If not then please install it.

* Git (git) must also be installed on your system. If you are using windows then please install git. 

* PHP version >= 7.0


<h2>Configuring the Project</h2>


*  open terminal and write "git clone https://gitlab.com/akuks/semantsf.git -b development "
<br>

* Rename .env.example to .env and 
    * Modify the database settings as <br>
    <br>
        DB_CONNECTION=mysql<br>
        DB_HOST=127.0.0.1<br>
        DB_PORT=3306<br>
        DB_DATABASE=YOUR_DATABASE_NAME <br>
        DB_USERNAME=DATABASE_USERNAME <br>
        DB_PASSWORD= DATABASE_PASSWORD if configured otherwise leave it<br>
<br>

* Regenrate the application key "php artisan key:generate"<br>

* Run database migrations "php artisan migrate"

* Run  development Server "php artisan serve" and open the browser to view the 
application at http://127.0.0.1:8000/ 
